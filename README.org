#+TITLE: Boxy Info

*This project has moved [[https://gitlab.com/grinn.amy/boxy-info][here]]*

View Info manuals as a boxy diagram.

* Installation

  First, [[https://gitlab.com/grinn.amy/boxy-info/-/releases][download the latest release]] (=boxy-info-x-y-z.el=)

  Then, =package-install-file RET boxy-info-x-y-z.el RET=

  Or with straight:

  #+begin_src emacs-lisp
    (use-package boxy-info
      :straight (boxy-info :type git :host gitlab :repo "grinn.amy/boxy-info")
      :config
      (define-key Info-mode-map (kbd "M-i") 'boxy-info))
  #+end_src

* Usage
  
** =boxy-info=
   To view an Info manual as a boxy diagram, use the command
   =boxy-info=.

   Suggested keybinding:
   #+begin_src emacs-lisp
     (global-set-key (kbd "C-h u") 'boxy-info)
   #+end_src

   [[file:demo/boxy-info.gif]]

   Standard Emacs movement commands will navigate by boxes instead of
   words or characters. Clicking on a box or pressing RET will take
   you to that node in the manual. If a box has a double outline, it
   can be expanded by hovering over it with the cursor and pressing
   TAB.

* License
  GPLv3
  
* Development
  
** Setup
   Install [[https://github.com/doublep/eldev#installation][eldev]]
** Commands:
*** =eldev lint=
    Lint the =boxy-info.el= file
*** =eldev compile=
    Test whether ELC has any complaints
*** =eldev package=
    Creates a dist folder with =boxy-info-<version>.el=
